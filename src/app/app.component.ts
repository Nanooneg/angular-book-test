import { Component } from '@angular/core';
import * as firebase from "firebase";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor() {
    // Your web app's Firebase configuration
    const firebaseConfig = {
      apiKey: "AIzaSyDVoV1KpQo6ozswuv953b-QmWtOpMshh4E",
      authDomain: "angular-book-4f43f.firebaseapp.com",
      databaseURL: "https://angular-book-4f43f.firebaseio.com",
      projectId: "angular-book-4f43f",
      storageBucket: "angular-book-4f43f.appspot.com",
      messagingSenderId: "1082528540932",
      appId: "1:1082528540932:web:afb46fb3a75e2c6384c851",
      measurementId: "G-E6MJJRG5FL"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
  }
}
